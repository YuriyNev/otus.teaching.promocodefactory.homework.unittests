﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Namotion.Reflection;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners;

public class SetPartnerPromoCodeLimitAsyncTests
{
    private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
    private readonly PartnersController _partnersController;
        
    public SetPartnerPromoCodeLimitAsyncTests()
    {
        var fixture = new Fixture().Customize(new AutoMoqCustomization());
        _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
        _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
    }
        
    public Partner CreateBasePartner()
    {
        var partner = new Partner()
        {
            Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
            Name = "Суперигрушки",
            IsActive = true,
            PartnerLimits = new List<PartnerPromoCodeLimit>()
            {
                new PartnerPromoCodeLimit()
                {
                    Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                    CreateDate = new DateTime(2020, 07, 9),
                    EndDate = new DateTime(2020, 10, 9),
                    Limit = 100
                }
            }
        };

        return partner;
    }

    [Fact]
    public async Task SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnNotFound()
    {
        // Arrange
        var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
        var codeLimitRequest = new SetPartnerPromoCodeLimitRequest
        {
            Limit = 4,
            EndDate = new DateTime(2023, 1, 1),
        };
        
        Partner partner = null;

        _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
            .ReturnsAsync(partner);

        // Act
        var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, codeLimitRequest);
 
        // Assert
        result.Should().BeAssignableTo<NotFoundResult>();
    }
    
    [Fact]
    public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnsBadRequest()
    {
        // Arrange
        var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
        var partner = CreateBasePartner();
        partner.IsActive = false;
        var codeLimitRequest = new SetPartnerPromoCodeLimitRequest
        {
            Limit = 4,
            EndDate = new DateTime(2023, 1, 1),
        };
            
        _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
            .ReturnsAsync(partner);

        // Act
        var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, codeLimitRequest);
 
        // Assert
        result.Should().BeAssignableTo<BadRequestObjectResult>();
    }
    
    [Fact]
    public async void SetPartnerPromoCodeLimitAsync_ThenResetPromoCodeCount()
    {
        // Arrange
        var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
        var partner = CreateBasePartner();
        var codeLimitRequest = new SetPartnerPromoCodeLimitRequest
        {
            Limit = 4,
            EndDate = new DateTime(2023, 1, 1),
        };
            
        _partnersRepositoryMock
            .Setup(repo => repo.GetByIdAsync(partnerId))
            .ReturnsAsync(partner);
        
        _partnersRepositoryMock
            .Setup(repo => repo.GetAllAsync())
            .ReturnsAsync(new [] { partner });

        // Act
        var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, codeLimitRequest);
        var limitId = result.TryGetPropertyValue<Guid>("limitId");
        var actionResult = await _partnersController.GetPartnersAsync();
        var codeLimitResponse = (PartnerPromoCodeLimitResponse)(actionResult.Result as OkObjectResult)?.Value;
        // Assert
        result.Should().BeAssignableTo<BadRequestObjectResult>();
    }
}